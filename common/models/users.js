module.exports = function(Users) {
	Users.login = function(cb) {
    var currentDate = new Date();
    var response = currentDate;
	
    cb(null, response);
  };
	Users.remoteMethod(
    'login',
    {
      http: {path: '/login', verb: 'get'},
      returns: {arg: 'status', type: 'string'}
    }
  );

};
